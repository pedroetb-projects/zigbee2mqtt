# zigbee2mqtt

Bridge between Zigbee devices and MQTT broker

## Initial config

On first startup, you can set `ZIGBEE2MQTT_CONFIG_ADVANCED_NETWORK_KEY=GENERATE` to let service create new network key. Then, you must unset this variable or there will be errors when service restarts.
